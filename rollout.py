# -*- coding:utf-8 -*-

import os
import random
import math
import copy

import numpy as np

import torch
import torch.nn as nn
import torch.optim as optim

from tqdm import tqdm

class Rollout(object):
    """Roll-out policy"""
    def __init__(self, encoder, decoder, update_rate):
        self.ori_encoder = encoder
        self.own_encoder = copy.deepcopy(encoder)
        self.ori_decoder = decoder
        self.own_decoder = copy.deepcopy(decoder)
        self.update_rate = update_rate

    def get_reward(self, samples, enc_hidden, enc_outputs, rollout_num, discriminator, eos_token):
        """
        Args:
            samples: (batch_size, seq_len) input data
            enc_hidden: (batch_size, hidden_size) encoder final state
            enc_outputs: (batch_size, seq_len, hidden_size) encoder outputs
            rollout_num: roll-out number
            discriminator: discrimanator model
        """
        rewards = []
        batch_size = samples.size(0)
        seq_len = samples.size(1)
        for i in range(rollout_num):
            for l in range(1, seq_len):
                data = samples[:, 0:l]

                dec_samples = torch.zeros((samples.size(0), seq_len - l), dtype=torch.long)
                # print(samples.size(0), seq_len - l, dec_samples.size())
                dec_hidden = enc_hidden
                for j in range(0, l):
                    dec_softmax, dec_hidden = self.own_decoder(data[:, j], dec_hidden, enc_outputs)

                dec_input = data[:, l-1]
                for j in range(l, seq_len):
                    dec_softmax, dec_hidden, dec_input = self.own_decoder.sample(dec_input, dec_hidden, enc_outputs)
                    dec_samples[:, j-l] = dec_input
                    # print(dec_input)

                out_samples = torch.cat([data, dec_samples], dim=1)
                _, softmax = discriminator(out_samples)
                pred = torch.log(softmax)
                # print(pred.size())
                pred = pred.cpu().data[:,1].numpy()
                if i == 0:
                    rewards.append(pred)
                else:
                    for p in range(pred.shape[0]):
                        if samples[p, l-1] != eos_token:
                            rewards[l-1][p] += pred[p]

            # for the last token
            _, softmax = discriminator(samples)
            pred = torch.log(softmax)
            pred = pred.cpu().data[:, 1].numpy()
            if i == 0:
                rewards.append(pred)
            else:
                for p in range(pred.shape[0]):
                    if samples[p, l-1] != eos_token:
                        rewards[l-1][p] += pred[p]

        rewards = np.transpose(np.array(rewards)) / (1.0 * rollout_num)     # (batch_size * seq_len)
        return rewards

    def update_params(self):
        dic_enc = {}
        for name, param in self.ori_encoder.named_parameters():
            dic_enc[name] = param.data
        for name, param in self.own_encoder.named_parameters():
            if name.startswith('embedding'):
                param.data = dic_enc[name]
            else:
                param.data = self.update_rate * param.data + (1 - self.update_rate) * dic_enc[name]

        dic_dec = {}
        for name, param in self.ori_decoder.named_parameters():
            dic_dec[name] = param.data
        for name, param in self.own_decoder.named_parameters():
            if name.startswith('embedding'):
                param.data = dic_dec[name]
            else:
                param.data = self.update_rate * param.data + (1 - self.update_rate) * dic_dec[name]