import torch
import torch.nn as nn
from torch.nn import functional as F

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

class SelfAttention(nn.Module):
	def __init__(self, output_size, hidden_size, vocab_size, embedding_length, weights=None):
		super(SelfAttention, self).__init__()

		self.output_size = output_size
		self.hidden_size = hidden_size
		self.vocab_size = vocab_size
		self.embedding_length = embedding_length
		self.weights = weights

		self.word_embeddings = nn.Embedding(vocab_size, embedding_length)
		if weights is not None:
			self.word_embeddings.weights = nn.Parameter(weights)
		self.dropout = 0.8
		self.bilstm = nn.LSTM(embedding_length, hidden_size, dropout=self.dropout, bidirectional=True)
		self.W_s1 = nn.Linear(hidden_size * 2, 350)
		self.W_s2 = nn.Linear(350, 30)
		self.fc_layer = nn.Linear(hidden_size * 2 * 30, 2000)
		self.label = nn.Linear(2000, output_size)

	def attention_net(self, lstm_output):
		attn_weight_matrix = self.W_s2(F.tanh(self.W_s1(lstm_output)))
		attn_weight_matrix = attn_weight_matrix.permute(0, 2, 1)
		attn_weight_matrix = F.softmax(attn_weight_matrix, dim=2)
		return attn_weight_matrix

	def forward(self, input_sentences):
		input = self.word_embeddings(input_sentences)
		h_0, c_0 = self.init_hidden(input.size(1))
		output, (h_n, c_n) = self.bilstm(input, (h_0, c_0))
		attn_weight_matrix = self.attention_net(output)
		hidden_matrix = torch.bmm(attn_weight_matrix, output)
		fc_out = self.fc_layer(hidden_matrix.view(-1, hidden_matrix.size()[1]*hidden_matrix.size()[2]))
		logits = self.label(fc_out)
		pred = F.softmax(logits, dim=1)

		return logits, pred

	def init_hidden(self, batch_size):
		h = torch.zeros((2, batch_size, self.hidden_size), device=device)
		c = torch.zeros((2, batch_size, self.hidden_size), device=device)
		return h, c
