# -*- coding: utf-8 -*-

import os
import random

import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F

class CNN(nn.Module):
    """A CNN for text classification
    architecture: Embedding >> Convolution >> Max-pooling >> Softmax
    """

    def __init__(self, num_classes, vocab_size, embed_size, filter_sizes, num_filters, dropout):
        super(CNN, self).__init__()
        self.embedding = nn.Embedding(vocab_size, embed_size)
        self.convs = nn.ModuleList([
            nn.Conv2d(1, n, (f, embed_size)) for (n, f) in zip(num_filters, filter_sizes)
        ])
        self.highway = nn.Linear(sum(num_filters), sum(num_filters))
        self.dropout = nn.Dropout(p=dropout)
        self.linear = nn.Linear(sum(num_filters), num_classes)
        self.softmax = nn.Softmax(dim=1)
        self.init_parameters()

    def forward(self, x):
        """
        Args:
            x: (batch_size * seq_len)
        """
        embedded = self.embedding(x).unsqueeze(1)                               # (batch_size * 1 * seq_len * embed_size)
        convs = [F.relu(conv(embedded)).squeeze(3) for conv in self.convs]      # (batch_size * num_filter * length)
        pools = [F.max_pool1d(conv, conv.size(2)).squeeze(2) for conv in convs] # (batch_size * num_filter)
        pred = torch.cat(pools, 1)                                              # (batch_size * num_filters_sum)
        highway = self.highway(pred)
        score = torch.sigmoid(highway) *  F.relu(highway) + (1. - torch.sigmoid(highway)) * pred
        logits = self.linear(self.dropout(score))
        softmax = self.softmax(logits)
        return logits, softmax

    def init_parameters(self):
        for param in self.parameters():
            param.data.normal_(0, 0.1)

    def set_emb_weights(self, embedding):
        self.embedding.weight = nn.Parameter(embedding)
