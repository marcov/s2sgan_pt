# _*_ coding: utf-8 _*_

import torch
import torch.nn as nn
from torch.nn import functional as F
import numpy as np

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

class AttentionLSTMModel(torch.nn.Module):
	def __init__(self, output_size, hidden_size, vocab_size, embedding_length, weights=None):
		super(AttentionLSTMModel, self).__init__()

		self.output_size = output_size
		self.hidden_size = hidden_size
		self.vocab_size = vocab_size
		self.embedding_length = embedding_length

		self.word_embeddings = nn.Embedding(vocab_size, embedding_length)
		if weights is not None:
			self.word_embeddings.weights = nn.Parameter(weights)
		self.lstm = nn.LSTM(embedding_length, hidden_size)
		self.label = nn.Linear(hidden_size, output_size)

	def attention_net(self, lstm_output, final_state):
		hidden = final_state.squeeze(0)
		attn_weights = torch.bmm(lstm_output, hidden.unsqueeze(2)).squeeze(2)
		soft_attn_weights = F.softmax(attn_weights, 1)
		new_hidden_state = torch.bmm(lstm_output.transpose(1, 2), soft_attn_weights.unsqueeze(2)).squeeze(2)
		return new_hidden_state

	def forward(self, input_sentences):
		input = self.word_embeddings(input_sentences)
		input = input.permute(1, 0, 2)
		h_0, c_0 = self.init_hidden(input.size(1))
		output, (final_hidden_state, final_cell_state) = self.lstm(input, (h_0, c_0)) 	# final_hidden_state.size() = (1, batch_size, hidden_size)
		output = output.permute(1, 0, 2) 												# (batch_size, num_seq, hidden_size)
		attn_output = self.attention_net(output, final_hidden_state)
		logits = self.label(attn_output)
		pred = F.softmax(logits, dim=1)
		return logits, pred

	def init_hidden(self, batch_size):
		h_0 = torch.zeros((1, batch_size, self.hidden_size), device=device)
		c_0 = torch.zeros((1, batch_size, self.hidden_size), device=device)
		return h_0, c_0