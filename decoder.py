# -*- coding: utf-8 -*-

import os
import random
import math

import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.nn.utils.rnn as rnn

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

class Attn(nn.Module):
    def __init__(self, hidden_size):
        super(Attn, self).__init__()
        self.hidden_size = hidden_size
        self.attn = nn.Linear(self.hidden_size * 2, hidden_size)
        self.v = nn.Parameter(torch.rand(hidden_size, device=device))
        stdv = 1. / math.sqrt(self.v.size(0))
        self.v.data.normal_(mean=0, std=stdv)

    def forward(self, hidden, enc_outputs):
        """
        Args:
            hidden: (n_layers*directions?, batch_size, hidden_size) previous hidden state of the decoder
            enc_outputs: encoder outputs from Encoder, in shape (B,T,H)
        """
        max_len = enc_outputs.size(1)
        this_batch_size = enc_outputs.size(0)
        H = hidden.repeat(max_len, 1, 1).transpose(0, 1)
        attn_energies = self.score(H, enc_outputs)              # compute attention score
        return F.softmax(attn_energies, dim=-1).unsqueeze(1)     # normalize with softmax

    def score(self, hidden, enc_outputs):
        energy = torch.tanh(self.attn(torch.cat([hidden, enc_outputs], 2)))     # (batch_size, seq_len, hidden_size)
        energy = energy.transpose(2, 1)                                         # (batch_size, hidden_size, hidden_size)
        v = self.v.repeat(enc_outputs.data.shape[0],1).unsqueeze(1)             # (batch_size, 1, hidden_size)
        energy = torch.bmm(v,energy)                                            # (batch_size, 1 ,hidden_size)
        return energy.squeeze(1)                                                # (batch_size, hidden_size)

class Decoder(nn.Module):
    def __init__(self, vocab_size, embed_size, hidden_size, n_layers=1, dropout=0.1, bidirectional=False):
        super(Decoder, self).__init__()
        # Parameters
        self.hidden_size = hidden_size * 2 if bidirectional else hidden_size
        self.embed_size = embed_size
        self.vocab_size = vocab_size
        self.n_layers = n_layers
        self.bidirectional = bidirectional
        # Layers
        self.embedding = nn.Embedding(vocab_size, embed_size)
        self.dropout = nn.Dropout(dropout)
        self.attn = Attn(self.hidden_size)
        self.gru = nn.GRU(embed_size + self.hidden_size, self.hidden_size)
        self.linear = nn.Linear(self.hidden_size, vocab_size)
        self.softmax = nn.LogSoftmax(dim=-1)

        self.init_params()

    def step(self, input, hidden, enc_outputs):
        """
        Args:
            input: (batch_size) word input for current time step
            hidden: (n_layers * directions, batch_size, hidden_size) last hidden stat of the decoder
            enc_outputs: (batch_size, seq_len, hidden_size) encoder outputs
        Note: we run this one step at a time
        """
        embedded = self.embedding(input).view(1, input.size(0), -1)     # (1, batch_size, embed_size)
        embedded = self.dropout(embedded)

        attn_weights = self.attn(hidden[-1], enc_outputs)
        context = attn_weights.bmm(enc_outputs)                         # (batch_size, 1, embed_size)
        context = context.permute(1, 0, 2)                              # (1, batch_size, embed_size)

        rnn_input = torch.cat((embedded, context), 2)
        output, hidden = self.gru(rnn_input, hidden)
        output = output.squeeze(0)                                      # (batch_size, embed_size)

        logits = self.linear(output)
        softmax = self.softmax(logits)
        return softmax, hidden

    def forward(self, input, hidden, enc_outputs):
        """
        Args:
            input: (batch_size) word input for current time step
            hidden: (n_layers * directions, batch_size, hidden_size) last hidden stat of the decoder
            enc_outputs: (batch_size, seq_len, hidden_size) encoder outputs
        Note: we run this one step at a time
        """
        embedded = self.embedding(input).view(1, input.size(0), -1)     # (1, batch_size, embed_size)
        embedded = self.dropout(embedded)

        attn_weights = self.attn(hidden[-1], enc_outputs)
        context = attn_weights.bmm(enc_outputs)                         # (batch_size, 1, embed_size)
        context = context.permute(1, 0, 2)                              # (1, batch_size, embed_size)

        rnn_input = torch.cat((embedded, context), 2)
        output, hidden = self.gru(rnn_input, hidden)
        output = output.squeeze(0)                                      # (batch_size, embed_size)

        logits = self.linear(output)
        softmax = self.softmax(logits)
        return softmax, hidden
        # batch_size = input.size(0)
        # predictions = torch.zeros((batch_size, steps, self.vocab_size), device=device)  # (batch_size, max_batch_len, vocab_size)
        # for i in range(steps):
        #     softmax, hidden = self.step(input[:, i], hidden, enc_outputs)
        #     predictions[:, i, :] = softmax

        # return predictions, hidden

    def init_params(self):
        for param in self.parameters():
            param.data.normal_(0, 0.1)

    def sample(self, input, hidden, enc_outputs):
        embedded = self.embedding(input).view(1, input.size(0), -1)     # (1, batch_size, embed_size)

        attn_weights = self.attn(hidden[-1], enc_outputs)
        context = attn_weights.bmm(enc_outputs)                         # (batch_size, 1, embed_size)
        context = context.permute(1, 0, 2)                              # (1, batch_size, embed_size)

        rnn_input = torch.cat((embedded, context), 2)
        output, hidden = self.gru(rnn_input, hidden)
        output = output.squeeze(0)                                      # (batch_size, embed_size)

        logits = self.linear(output) / 0.7
        softmax = F.softmax(logits, dim=1)
        sample = softmax.multinomial(1).squeeze(1)
        return softmax, hidden, sample
