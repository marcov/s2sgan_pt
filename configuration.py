import argparse

def init_parser():
	parser = argparse.ArgumentParser(description='Training Parameter')

	# save - resume
	parser.add_argument('--save', type=str, default=None)
	parser.add_argument('--resume', type=str, default=None)
	parser.add_argument('--gdrive', type=str, default=None)
	parser.add_argument('--checkpoint_interval', type=int, default=10)

	# training params
	parser.add_argument('--gan_epochs', type=int, default=11)
	parser.add_argument('--batch_size', type=int, default=16)
	parser.add_argument('--sequence_len', type=int, default=23)

	# dataset params
	parser.add_argument('--train', type=str, default="image_coco/coco_train.tsv")
	parser.add_argument('--test', type=str, default="image_coco/coco_test.tsv")
	parser.add_argument('--real_data', type=str, default="save/coco_positive.data")
	parser.add_argument('--fake_data', type=str, default="save/coco_negative.data")
	parser.add_argument('--eval_data', type=str, default="save/coco_eval.data")

	# generator params
	parser.add_argument('--g_pre_epochs', type=int, default=20)
	parser.add_argument('--g_epochs', type=int, default=1)
	parser.add_argument('--g_embed_size', type=int, default=100)
	parser.add_argument('--g_hidden_size', type=int, default=100)
	parser.add_argument('--g_pre_embed', type=int, default=0)
	parser.add_argument('--g_bidirectional', type=bool, default=True)

	# discrminator params
	parser.add_argument('--d_pre_epochs', type=int, default=10)
	parser.add_argument('--d_epochs', type=int, default=3)
	parser.add_argument('--d_embed_size', type=int, default=100)
	parser.add_argument('--d_filter_sizes', type=str, default="2 3 5")
	parser.add_argument('--d_num_filters', type=str, default="100 200 200")
	parser.add_argument('--d_dropout', type=float, default=0.75)
	parser.add_argument('--d_num_class', type=int, default=2)
	parser.add_argument('--d_type', type=int, default=0)

	return parser