# -*- coding:utf-8 -*-

import os
import random
import math
import sys

import argparse

import numpy as np

import torch
import torchtext

from encoder import Encoder
from decoder import Decoder
from discriminator.cnn import CNN
from discriminator.attention_lstm import AttentionLSTMModel
from discriminator.attention_indRNN import IndRNNModel
from discriminator.self_attention import SelfAttention
from rollout import Rollout
from data_iter import DisDataIter

from eval.test_metrics import compute_bleu
from eval.plot_metrics import plot_metrics
# ================== Parameter Definition =================

parser = argparse.ArgumentParser(description='Training Parameter')

# Model parameters.
parser.add_argument('--checkpoint', type=str, default='')
parser.add_argument('--generated_num', type=int, default=20)
parser.add_argument('--sequence_len', type=int, default=20)
parser.add_argument('--discriminate', action='store_true')
parser.add_argument('--classify', type=bool, default=False)
parser.add_argument('--seed', type=int, default=234)
parser.add_argument('--input', type=str, default=None)
parser.add_argument('--test', type=str, default=None)
parser.add_argument('--name', type=str, default='None')

opt = parser.parse_args()

device = 'cuda' if torch.cuda.is_available() else 'cpu'

real_count = 0

def ids_to_text_real(vocabulary, samples, pred):
    global real_count
    output = open('eval/generated.txt', 'a')
    pad = vocabulary.stoi.get('<pad>')
    for i, line in zip(range(0, len(samples)), samples):
        if torch.exp(pred[i]).argmax().item() == 0:
            continue
        for token in line:
            if int(token) == pad:
                break
            print(vocabulary.itos[token], end=' ')
            output.write(vocabulary.itos[token] + ' ')
        print(str(torch.exp(pred[i]).multinomial(1).item()) + '')
        output.write('\n')
        real_count += 1
        if real_count == opt.generated_num:
            break
    output.close()

# generate text
def ids_to_text(vocabulary, samples):
    i = 0
    output = open('eval/generated.txt', 'w')
    pad = vocabulary.stoi.get('<pad>')
    for line in samples:
        for token in line:
            if int(token) == pad:
                break
            print(vocabulary.itos[token], end=' ')
            output.write(vocabulary.itos[token] + ' ')
        print('')
        output.write('\n')
    output.close()

# seed to tensor
def text_to_tensor(x, vocabulary, num):
    words = x.split()
    sample = []
    for word in words:
        sample.append(vocabulary.stoi.get(word, 0))
    samples = []
    for i in range(num):
        samples.append(sample)
    return torch.tensor(samples, dtype=torch.long, device=device)

def process_dataset(train_dataset, test_dataset):

    text_field = torchtext.data.Field(sequential=True, batch_first=True, include_lengths=True, eos_token='<eos>')
    gen_data = torchtext.data.TabularDataset(
        path=train_dataset, format='tsv',
        fields=[('source', text_field), ('target', text_field)]
        )

    text_field.build_vocab(gen_data)

    test_data = torchtext.data.TabularDataset(
        path=test_dataset, format='tsv',
        fields=[('source', text_field), ('target', text_field)]
        )

    return gen_data, test_data, text_field.vocab

def main():
    global real_count

    # _, _, vocabulary = process_dataset(
    #         'image_coco/coco_train.1.tsv',
    #         'image_coco/coco_test.1.tsv')

    # dic_ckpt = os.path.join(opt.checkpoint, 'dic')
    # torch.save(vocabulary, dic_ckpt)
    # exit()

    enc_ckpt = os.path.join(opt.checkpoint, 'enc')
    dec_ckpt = os.path.join(opt.checkpoint, 'dec')
    dis_ckpt = os.path.join(opt.checkpoint, 'dis')
    dic_ckpt = os.path.join(opt.checkpoint, 'dic')

    encoder, _, _ = torch.load(enc_ckpt, map_location=device)
    decoder, _, _ = torch.load(dec_ckpt, map_location=device)
    discriminator, _, _ = torch.load(dis_ckpt, map_location=device)
    vocabulary = torch.load(dic_ckpt, map_location=device)

    torch.manual_seed(opt.seed)

    encoder.eval()
    decoder.eval()
    discriminator.eval()

    sequence_len = opt.sequence_len
    generated_num = opt.generated_num
    x = text_to_tensor(opt.input, vocabulary, generated_num) if opt.input is not None else None

    while True:
        seq_str = input('type input:')
        seq = seq_str.strip().split()

        enc_input = torch.zeros((1, len(seq)), dtype=torch.long, device=device)
        for i in range(len(seq)):
            enc_input[0, i] = vocabulary.stoi.get(seq[i], 0)
        enc_outputs, dec_hidden = encoder(enc_input, [len(seq)])
        dec_sample = torch.zeros(1, dtype=torch.long, device=device)
        sample = torch.zeros((1, sequence_len), device=device)
        for i in range(sequence_len):
            dec_softmax , dec_hidden, dec_sample = decoder.sample(dec_sample, dec_hidden, enc_outputs)
            sample[0, i] = dec_sample# torch.argmax(dec_softmax, dim=1)

        res = ' '.join([vocabulary.itos[int(i.item())] for i in sample[0]])
        print(res)

    if opt.test is not None:
        with open('eval/log_metric/' + opt.name + '.csv', 'w') as f:
            for i in range(2, 6):
                result_bleu, result_posbleu, result_selfbleu = compute_bleu(i, 'eval/generated.txt', opt.test, 'bleu pos self')
                f.write(str(i) + ';' + str(result_bleu) + ';' + str(result_posbleu) + ';' + str(result_selfbleu) + ';\n')
                print('BLEU ' , i , ': ', str(result_bleu))
                print('POSBLEU ' , i , ': ', str(result_posbleu))
                print('SELFBLEU ' , i , ': ', str(result_selfbleu))
            for i in range(6, 8):
                result_bleu, result_posbleu, result_selfbleu = compute_bleu(i, 'eval/generated.txt', opt.test, 'pos')
                f.write(str(i) + ';' + str(result_bleu) + ';' + str(result_posbleu) + ';' + str(result_selfbleu) + ';\n')
                print('BLEU ' , i , ': ', str(result_bleu))
                print('POSBLEU ' , i , ': ', str(result_posbleu))
                print('SELFBLEU ' , i , ': ', str(result_selfbleu))
        plot_metrics('eval/log_metric', 'eval/plots')

if __name__ == '__main__':
    main()