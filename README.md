# SeqGAN
SeqGan pytorch implementation based on https://github.com/ZiJianZhao/SeqGAN-PyTorch


## How to run 
### Run training
```
python main.py --target_data=image_coco/image_coco.txt 

```
### Generate text and evaluate
```
python generate.py --checkpoint <checkpoint_path> --test image_coco/test_coco.txt

```
### Embedding statistics
```
python test_emb_glove.py --checkpoint <checkpoint_path>
python test_emb.py --checkpoint1 <reference> --checkpoint2 <test>

```

## Changelog
- added checkpoints, logs, gdrive upload
- added glove embeddings
- generate.py to generate text from saved model
- test_emb.py compute the mean_distance and mean_difference for each embedding
- test_emb_glove.py compute the distance from glove embeddings

## Arguments main.py
parser.add_argument('--save', type=str, default='') <br>
parser.add_argument('--resume', type=str, default='') <br>

parser.add_argument('--total_batch', type=int, default=30) <br>
parser.add_argument('--batch_size', type=int, default=2) <br>
parser.add_argument('--generated_num', type=int, default=200) <br>

parser.add_argument('--g_pre_epochs', type=int, default=4) <br>
parser.add_argument('--g_epochs', type=int, default=1) <br>
parser.add_argument('--g_steps', type=int, default=1) <br>
parser.add_argument('--g_emb_dim', type=int, default=300) <br>
parser.add_argument('--g_hidden_dim', type=int, default=300) <br>
parser.add_argument('--g_sequence_len', type=int, default=16) <br>

parser.add_argument('--d_pre_epochs', type=int, default=1) <br>
parser.add_argument('--d_epochs', type=int, default=3) <br>
parser.add_argument('--d_steps', type=int, default=5) <br>
parser.add_argument('--d_emb_dim', type=int, default=64) <br>
parser.add_argument('--d_dropout', type=float, default=0.75) <br>
parser.add_argument('--d_num_class', type=int, default=2) <br>

parser.add_argument('--target_data', type=str, default="./image_coco/sample.txt") <br>
parser.add_argument('--train_data', type=str, default="train.data") <br>
parser.add_argument('--gene_data', type=str, default="gene.data") <br>
parser.add_argument('--eval_data', type=str, default="eval.data") <br>
parser.add_argument('--embeddings', type=str, default=None) <br>

parser.add_argument('--embedding', type=int, default=None) <br>
parser.add_argument('--gdrive', type=str, default='') <br>