# -*- coding:utf-8 -*-

import os
import random
import math

import tqdm

import numpy as np
import torch
import torch.nn as nn

class DisDataIter(object):

    def __init__(self, real_data_file, fake_data_file, pad_token, batch_size, sequence_len):
        super(DisDataIter, self).__init__()
        self.batch_size = batch_size
        self.sequence_len = sequence_len
        self.pad_token = pad_token

        self.real_data_lis = self.read_file(real_data_file)
        self.fake_data_lis = self.read_file(fake_data_file)

        self.data = self.real_data_lis + self.fake_data_lis
        self.labels = [1 for _ in range(len(self.real_data_lis))] +\
                        [0 for _ in range(len(self.fake_data_lis))]
        self.pairs = list(zip(self.data, self.labels))

        self.data_num = len(self.pairs)
        self.indices = range(self.data_num)
        self.num_batches = int(math.ceil(float(self.data_num)/self.batch_size))
        self.idx = 0

        random.shuffle(self.pairs)

    def __len__(self):
        return self.num_batches

    def __iter__(self):
        return self

    def __next__(self):
        return self.next()

    def reset(self):
        self.idx = 0
        random.shuffle(self.pairs)

    def next(self):
        if self.idx >= self.data_num or self.data_num - self.idx < self.batch_size:
            raise StopIteration
        index = self.indices[self.idx:self.idx+self.batch_size]
        length = [min(len(self.data[i])+1, self.sequence_len) for i in index]
        max_len = max(length)
        data, label = zip(*[self.pairs[i] for i in index])
        padded_data = np.ones((self.batch_size, max_len)) * self.pad_token
        for i in range(0, self.batch_size):
            padded_data[i, 0:length[i]-1] = self.data[i+self.idx][:length[i]-1]
        data = torch.tensor(np.asarray(padded_data, dtype='int64'), dtype=torch.long)
        label = torch.tensor(np.asarray(label, dtype='int64'), dtype=torch.long)
        self.idx += self.batch_size
        return data, label

    def read_file(self, data_file):
        with open(data_file, 'r') as f:
            lines = f.readlines()
        lis = []
        for line in lines:
            l = line.strip().split(' ')
            l = [int(s) for s in l]
            lis.append(l)
        return lis

