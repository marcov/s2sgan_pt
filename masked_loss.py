import torch
import torch.nn as nn
import torch.nn.functional as F

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

class MaskedLoss(nn.Module):

    def __init__(self):
        super(MaskedLoss, self).__init__()

    def _sequence_mask(self, sequence_length, max_len=None):
        if max_len is None:
            max_len = sequence_length.data.max()
        batch_size = sequence_length.size(0)
        seq_range = torch.arange(0, max_len).long()
        seq_range_expand = seq_range.unsqueeze(0).expand(batch_size, max_len).to(device)
        seq_length_expand = (sequence_length.unsqueeze(1).expand_as(seq_range_expand))
        return seq_range_expand < seq_length_expand

    def forward(self, prediction, target, length, reduce='mean', rewards=None):
        """
        Args:
            prediction: A Variable containing a FloatTensor of size
                (batch, max_len, num_classes) which contains the
                log normalized probability for each class.
            target: A Variable containing a LongTensor of size
                (batch, max_len) which contains the index of the true
                class for each corresponding step.
            length: A Variable containing a LongTensor of size (batch,)
                which contains the length of each data in a batch.
        """

        prediction_flat = prediction.view(-1, prediction.size(-1))                  # (batch_size * seq_len, num_classes)
        target_flat = target.view(-1, 1)                                            # (batch_size * seq_len, 1)
        losses_flat = -torch.gather(prediction_flat, dim=1, index=target_flat)      # (batch_size * seq_len, 1)
        losses = losses_flat.view(*target.size())                                   # (batch_size, seq_len)
        mask = self._sequence_mask(sequence_length=length, max_len=target.size(1))  # (batch_size, seq_len)
        losses = losses * mask.float()
        if rewards is not None:
            losses = losses.contiguous().view(1, -1)
            losses = - losses * rewards                                             # (batch_size * seq_len)
        if reduce == 'mean':
            loss = losses.sum() / length.float().sum()
        elif reduce == 'sum':
            loss = losses.sum()
        return loss