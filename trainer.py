# -*- coding:utf-8 -*-
import os
import math
import numpy as np

import torch
import torch.nn as nn
import torch.optim as optim

from encoder import Encoder
from decoder import Decoder

from discriminator.cnn import CNN
from discriminator.self_attention import SelfAttention
from discriminator.attention_lstm import AttentionLSTMModel
from discriminator.attention_indRNN import IndRNNModel

from data_iter import DisDataIter
from rollout import Rollout
from masked_loss import MaskedLoss

from pathlib import Path
from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
from oauth2client.client import GoogleCredentials

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

class Trainer():

	def __init__(self, opt, vocab_size, resume=False):
		# Trainer Parameters
		self.save_path = opt.save
		self.resume_path = opt.resume
		self.drive_uri = opt.gdrive
		if self.drive_uri is not None:
			self.gauth, self.drive = self._init_drive()

		self.vocab_size = vocab_size

		if resume is False:
			self._init_trainer(opt, vocab_size)
		else:
			self._restore_trainer()

	def _init_trainer(self, opt, vocab_size):
		# Genrator Parameters
		g_embed_size = opt.g_embed_size
		g_hidden_size = opt.g_hidden_size
		g_bidirectional = opt.g_bidirectional

		# Discriminator Parameters
		d_embed_size = opt.d_embed_size
		d_filter_sizes = list(map(int, opt.d_filter_sizes.split(' ')))
		d_num_filters = list(map(int, opt.d_num_filters.split(' ')))
		d_dropout = opt.d_dropout
		d_num_class = opt.d_num_class
		d_type = opt.d_type

		self.epoch = 1

		# Generator -> Seq2Seq: Encoder - Decoder
		self.encoder = Encoder(vocab_size, g_embed_size, g_hidden_size, bidirectional=g_bidirectional).to(device)
		self.enc_optimizer = optim.Adam(self.encoder.parameters(), lr=1e-3)
		self.decoder = Decoder(vocab_size, g_embed_size, g_hidden_size, bidirectional=g_bidirectional).to(device)
		self.dec_optimizer = optim.Adam(self.decoder.parameters(), lr=1e-3)
		self.gen_criterion = MaskedLoss().to(device)
		self.gen_test_criterion = nn.NLLLoss(reduction='sum').to(device)

		# Discriminator
		if d_type == 0:
			self.discriminator = CNN(d_num_class, vocab_size, d_embed_size, d_filter_sizes, d_num_filters, d_dropout).to(device)
		elif d_type == 1:
			self.discriminator = SelfAttention(d_num_class, d_embed_size, vocab_size, d_embed_size).to(device)
		elif d_type == 2:
			self.discriminator = AttentionLSTMModel(d_num_class, d_embed_size, vocab_size, d_embed_size).to(device)
		elif d_type == 3:
			self.discriminator = IndRNNModel(d_num_class, d_embed_size, vocab_size, d_embed_size).to(device)
		self.dis_criterion = nn.CrossEntropyLoss().to(device)
		self.dis_optimizer = optim.Adam(self.discriminator.parameters(), lr=1e-4)

		self.rollout = Rollout(self.encoder, self.decoder, 0.8)
		self.gen_gan_criterion = MaskedLoss()
		self.gen_gan_optmizer = optim.Adam(self.decoder.parameters())

	def _restore_trainer(self):
		if self.resume_path is not None:
			resume_path = self.resume_path
			if not Path(resume_path).exists():
				print('checkpoint path does not exist')
				return 0
			enc_ckpt = os.path.join(resume_path, 'enc')
			dec_ckpt = os.path.join(resume_path, 'dec')
			dis_ckpt = os.path.join(resume_path, 'dis')
			gan_ckpt = os.path.join(resume_path, 'gan')

			self.encoder, self.gen_criterion, self.enc_optimizer = torch.load(enc_ckpt, map_location=device)
			self.decoder, self.gen_criterion, self.dec_optimizer = torch.load(dec_ckpt, map_location=device)
			self.discriminator, self.dis_criterion, self.dis_optimizer = torch.load(dis_ckpt, map_location=device)
			self.rollout, self.gen_gan_criterion, self.gen_gan_optmizer, self.epoch = torch.load(gan_ckpt, map_location=device)

	def train_generator(self, data_iterator):
		total_loss = []
		for batch in data_iterator:
			src_data = batch.source[0].to(device)   # (batch_size, max_batch_len)
			src_len = batch.source[1].to(device)    # (batch_size)
			tgt_data = batch.target[0].to(device)
			tgt_len = batch.target[1].to(device)

			# encode batch
			enc_outputs, enc_hidden = self.encoder(src_data, src_len)
			# prepare decoder parameters
			batch_size = tgt_data.size(0)
			steps = tgt_data.size(1)
			dec_input = torch.zeros((batch_size), dtype=torch.long, device=device)              # (batch_size)
			dec_hidden = enc_hidden                                                             # (batch_size, hidden_size)
			dec_predictions = torch.zeros((batch_size, steps, self.vocab_size), device=device) 	# (batch_size, max_batch_len, vocab_size)
			# decode TODO teacher forcing
			for i in range(steps):
				dec_softmax, dec_hidden = self.decoder(dec_input, dec_hidden, enc_outputs)
				dec_input = tgt_data[:, i]
				dec_predictions[:, i, :] = dec_softmax

			loss = self.gen_criterion(dec_predictions, tgt_data, tgt_len)
			total_loss.append(loss.item())

			self.enc_optimizer.zero_grad()
			self.dec_optimizer.zero_grad()
			loss.backward()
			self.enc_optimizer.step()
			self.dec_optimizer.step()
		return np.mean(total_loss)

	def train_discriminator(self, data_iterator):
		total_loss = []
		data_iterator.reset()
		for data, target in data_iterator:
			data = data.to(device)
			target = target.to(device)
			target = target.contiguous().view(-1)
			logits, softmax = self.discriminator(data)
			loss = self.dis_criterion(logits, target)
			total_loss.append(loss.item())
			self.dis_optimizer.zero_grad()
			loss.backward()
			self.dis_optimizer.step()
		return np.mean(total_loss)

	def train_gan(self, data_iterator, eos_token):

		for batch in data_iterator:
			src_data = batch.source[0].to(device)
			src_len = batch.source[1].to(device)
			tgt_data = batch.target[0].to(device)
			tgt_len = batch.target[1].to(device)

			# encode batch
			enc_outputs, enc_hidden = self.encoder(src_data, src_len)
			# prepare decoder params
			dec_input = torch.zeros(enc_outputs.size(0), dtype=torch.long, device=device)
			dec_hidden = enc_hidden
			dec_predictions = torch.zeros((tgt_data.size(0), tgt_data.size(1), self.vocab_size), device=device)
			max_batch_len = tgt_data.size(1)
			# samples from decoder
			samples = torch.zeros((tgt_data.size(0), tgt_data.size(1)), dtype=torch.long, device=device)
			# decode
			for i in range(max_batch_len):
				dec_softmax, dec_hidden, dec_input = self.decoder.sample(dec_input, dec_hidden, enc_outputs)
				samples[:, i] = dec_input
				dec_predictions[:, i, :] = dec_softmax

			rewards = self.rollout.get_reward(samples, enc_hidden, enc_outputs, 4, self.discriminator, eos_token)
			rewards = torch.tensor(rewards, device=device)
			rewards = torch.exp(rewards).contiguous().view((-1,))

			loss = self.gen_gan_criterion(dec_predictions, tgt_data, tgt_len, reduce='sum', rewards=rewards)

			self.gen_gan_optmizer.zero_grad()
			loss.backward()
			self.gen_gan_optmizer.step()
			self.rollout.update_params()
		return loss

	def get_epoch(self):
		return self.epoch

	# generate output/samples  TODO handle big file
	def generate(self, data_iterator):
		samples_iterator = []
		for batch in data_iterator:
			src_data = batch.source[0].to(device)
			src_len = batch.source[1].to(device)
			tgt_data = batch.target[0].to(device)
			tgt_len = batch.target[1].to(device)

			enc_outputs, enc_hidden = self.encoder(src_data, src_len)
			dec_sample = torch.zeros(enc_outputs.size(0), dtype=torch.long, device=device)
			dec_hidden = enc_hidden
			dec_samples = torch.zeros((tgt_data.size(0), tgt_data.size(1)), device=device)
			for i in range(tgt_data.size(1)):
				_ , dec_hidden, dec_sample = self.decoder.sample(dec_sample, dec_hidden, enc_outputs)
				dec_samples[:, i] = dec_sample

			samples_iterator.append(dec_samples)

		return samples_iterator

	def discriminate(self, data_iterator):
		result_real = []
		result_fake = []
		for data, target in data_iterator:
			data = data.to(device)
			target = target.to(device)
			target = target.contiguous().view(-1)
			score, pred = self.discriminator(data)

			for i in range(0, len(score)):
				label = score[i].argmax().item()
				if target[i].data == 1:
					result_real.append(label)
				if target[i].data == 0:
					result_fake.append(label)

		return result_real, result_fake

	# evaluate generator
	def eval_generator(self, data_iterator):
		total_loss = 0.0
		total_count = 0.0

		for batch in data_iterator:
			src_data = batch.source[0].to(device)
			src_len = batch.source[1].to(device)
			tgt_data = batch.target[0].to(device)
			tgt_len = batch.target[1].to(device)

			enc_outputs, enc_hidden = self.encoder(src_data, src_len)
			dec_sample = torch.zeros(enc_outputs.size(0), dtype=torch.long, device=device)
			dec_hidden = enc_hidden
			dec_predictions = torch.zeros((tgt_data.size(0), tgt_data.size(1), self.vocab_size), device=device)
			dec_samples = torch.zeros((tgt_data.size(0), tgt_data.size(1)), device=device)
			for i in range(tgt_data.size(1)):
				dec_softmax , dec_hidden, dec_sample = self.decoder.sample(dec_sample, dec_hidden, enc_outputs)
				dec_samples[:, i] = dec_sample
				dec_predictions[:, i, :] = dec_softmax

			loss = self.gen_criterion(dec_predictions, tgt_data, tgt_len, reduce='sum')
			total_loss += loss.item()
			total_count += torch.sum(tgt_len).item()
		return math.exp(total_loss / total_count)


	def _model_state(self, model, criterion, optimizer):
		return {
			'state_dict': model.state_dict(),
			'criterion': criterion,
            'optimizer' : optimizer.state_dict()
		}

	# %save_path%_%epoch%
	#  	| - enc	-> encoder: state_dict, criterion, optimizer
	#	| - dec	-> decoder: state_dict, criterion, optimizer
	# 	| - dis	-> discriminator: state_dict, criterion, optimizer
	#	| - gan	-> gan: rollout, criterion, optimizer, epoch
	def save(self, epoch, vocabulary):
		if self.save_path is not None:
			save_path = self.save_path + '_' + str(epoch)
			if not os.path.exists(save_path):
				os.mkdir(save_path)
			enc_ckpt = os.path.join(save_path, 'enc')
			dec_ckpt = os.path.join(save_path, 'dec')
			dis_ckpt = os.path.join(save_path, 'dis')
			gan_ckpt = os.path.join(save_path, 'gan')
			dic_ckpt = os.path.join(save_path, 'dic')
			torch.save([self.encoder, self.gen_criterion, self.enc_optimizer], enc_ckpt)
			torch.save([self.decoder, self.gen_criterion, self.dec_optimizer], dec_ckpt)
			torch.save([self.discriminator, self.dis_criterion, self.dis_optimizer], dis_ckpt)
			torch.save([self.rollout, self.gen_gan_criterion, self.gen_gan_optmizer, epoch], gan_ckpt)
			torch.save(vocabulary, dic_ckpt)
			# writer.add_embedding(decoder.get_emb(), metadata=corpus.dictionary.stoi.keys(), tag='Embeddings', global_step=0)
			if self.drive_uri is not None:
				os.system('zip -r %s.zip %s save runs logs_%s > /dev/null' % (save_path, save_path, self.save_path))
				self._upload_to_gdrive(self.drive_uri)
			return save_path

	def _init_drive(self):
		from google.colab import auth
		auth.authenticate_user()
		gauth = GoogleAuth()
		gauth.credentials = GoogleCredentials.get_application_default()
		drive = GoogleDrive(gauth)
		return gauth, drive

	def _upload_to_gdrive(self, path):
		if self.gauth.access_token_expired:
			self.gauth.Refresh()
			drive = GoogleDrive(self.gauth)
		uploaded = self.drive.CreateFile({"parents": [{"kind": "drive#fileLink", "id": self.drive_uri}]})
		uploaded.SetContentFile('%s.zip' % (path))
		uploaded.Upload()
		print('Uploaded file with ID {}'.format(uploaded.get('id')))

	def model_stats(self):
		encoder_params = sum(p.numel() for p in self.encoder.parameters())
		decoder_params = sum(p.numel() for p in self.decoder.parameters())
		discrim_params = sum(p.numel() for p in self.discriminator.parameters())
		total_params = encoder_params + decoder_params + discrim_params
		stats = 'Parameters stats: \n' + \
				' + Encoder: ' + str(encoder_params) + '\n' + \
				' + Decoder: ' + str(decoder_params) + '\n' + \
				' + Discrim: ' + str(discrim_params) + '\n' + \
				' = Total: ' + str(total_params)
		print(stats)

