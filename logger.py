import os

class Logger():

    def __init__(self, conf, save=None):
        os.system('mkdir -p logs')
        logs_path = 'logs'
        if save is not None:
            logs_path = logs_path + '_' + save
        if not os.path.exists(logs_path):
            os.mkdir(logs_path)
        self.conf = open('%s/conf_log.txt' % logs_path, 'a')
        self.gen = open('%s/gen_log.txt' % logs_path, 'a')
        self.plp = open('%s/plp_log.txt' % logs_path, 'a')
        self.dis = open('%s/dis_log.txt' % logs_path, 'a')
        self.gan = open('%s/gan_log.txt' % logs_path, 'a')
        self.disg = open('%s/disg_log.txt' % logs_path, 'a')

        self.conf.write(conf)
        self.gen.write('epoch;loss;\n')
        self.plp.write('epoch;loss;\n')
        self.dis.write('epoch;loss;\n')
        self.gan.write('iteration;loss;\n')
        self.disg.write('iteration;epoch;loss;\n')

    def write_gen(self, message):
        self.gen.write(message + ';\n')

    def write_plp(self, message):
        self.plp.write(message + ';\n')

    def write_dis(self, message):
        self.dis.write(message + ';\n')

    def write_gan(self, message):
        self.gan.write(message + ';\n')

    def write_dis_gan(self, message):
        self.disg.write(message + ';\n')

    def flush(self):
        self.conf.flush()
        os.fsync(self.conf.fileno())
        self.gen.flush()
        os.fsync(self.gen.fileno())
        self.plp.flush()
        os.fsync(self.plp.fileno())
        self.dis.flush()
        os.fsync(self.dis.fileno())
        self.gan.flush()
        os.fsync(self.gan.fileno())
        self.disg.flush()
        os.fsync(self.disg.fileno())

    def close(self):
        self.conf.close()
        self.gen.close()
        self.plp.close()
        self.dis.close()
        self.gan.close()
        self.disg.close()
