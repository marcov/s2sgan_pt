# -*- coding:utf-8 -*-
import os
import random
import sys
import numpy as np

import torch
import torchtext

from configuration import init_parser
from logger import Logger
from tensorboardX import SummaryWriter

from trainer import Trainer
from data_iter import DisDataIter

# ================== Parameter Definition =================
parser = init_parser()
opt = parser.parse_args()
print(opt)

# Training Paramters
GAN_EPOCHS = opt.gan_epochs
G_PRE_EPOCHS = opt.g_pre_epochs
D_PRE_EPOCHS = opt.d_pre_epochs
G_EPOCHS = opt.g_epochs
D_EPOCHS = opt.d_epochs

BATCH_SIZE = opt.batch_size
SEQUENCE_LEN = opt.sequence_len

TRAIN = opt.train
TEST = opt.test
POSITIVE = opt.real_data
NEGATIVE = opt.fake_data
EVAL_FILE = opt.eval_data

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# generate corpus from file
def process_dataset(train_dataset, test_dataset):

    text_field = torchtext.data.Field(sequential=True, batch_first=True, include_lengths=True, eos_token='<eos>')
    gen_data = torchtext.data.TabularDataset(
        path=train_dataset, format='tsv',
        fields=[('source', text_field), ('target', text_field)]
        )

    text_field.build_vocab(gen_data)

    test_data = torchtext.data.TabularDataset(
        path=test_dataset, format='tsv',
        fields=[('source', text_field), ('target', text_field)]
        )

    return gen_data, test_data, text_field.vocab

def generate_real(data_iterator):
    with open(POSITIVE, 'w') as f:
        for batch in data_iterator:
            tgt_data = batch.target[0].to(device)

            for row in tgt_data:
                string = ' '.join([str(int(s.item())) for s in row])
                f.write('%s\n' % string)

def generate_fake(samples_iterator):
    with open(NEGATIVE, 'w') as f:
        for samples in samples_iterator:
            for row in samples:
                string = ' '.join([str(int(s.data.cpu().numpy().tolist())) for s in row])  # check for padding
                f.write('%s\n' % string)

def generate_eval(vocabulary, result=None):
    i = 0
    pad = vocabulary.stoi.get('<pad>', -1)
    n = open(NEGATIVE, 'r')
    with open(EVAL_FILE, 'w') as e:
        for line in n.readlines():
            tokens = line.split(' ')
            for token in tokens:
                if int(token) == pad:
                    break
                e.write(vocabulary.itos[int(token)] + ' ')
            if result is not None and i < len(result):
                e.write('; ' + str(result[i]) + ';\n')
                i += 1
            else:
                e.write('; -1 ;\n')

def main():

    os.system('rm -r runs > /dev/null')
    writer = SummaryWriter()
    log = Logger(str(opt), opt.save)

    # Define corpus
    os.system('mkdir -p save')
    gen_data, test_data, vocabulary = process_dataset(TRAIN, TEST)
    gen_data_iter = torchtext.data.BucketIterator(gen_data, BATCH_SIZE, device=device,
            sort_key=lambda x: len(x.source), sort_within_batch=True,
            repeat=False, shuffle=True)
    eval_iter = torchtext.data.BucketIterator(test_data, BATCH_SIZE, device=device,
            sort_key=lambda x: len(x.source), sort_within_batch=True,
            repeat=False, shuffle=True)

    vocab_size = len(vocabulary.itos)
    pad_token = vocabulary.stoi.get('<pad>', -1)
    eos_token = vocabulary.stoi.get('<eos>', -1)
    generate_real(gen_data_iter)

    # restore trained decoder and discriminator
    if opt.resume is not None:
        trainer = Trainer(opt, vocab_size, resume=True)
    else:

        # init trainer
        trainer = Trainer(opt, vocab_size)
        trainer.model_stats()

        # pretrain generator
        print('Pretrain Generator ...')
        for epoch in range(G_PRE_EPOCHS):
            gen_data_iter.init_epoch()
            loss = trainer.train_generator(gen_data_iter)
            print('Epoch [%d] Model Loss: %f'% (epoch, loss))
            log.write_gen(str(epoch) + ';' + str(loss))
            writer.add_scalar('data/pre_g_loss', loss, epoch)

        # evaluate generator
        gen_data_iter.init_epoch()
        loss = trainer.eval_generator(gen_data_iter)
        print('Epoch [%d] Perplexity: %f' % (epoch, loss))
        log.write_plp(str(epoch) + ';' + str(loss))
        writer.add_scalar('data/perplexity', loss, epoch)

        # pretrain discriminator
        print('Pretrain Discriminator ...')
        gen_data_iter.init_epoch()
        dec_samples = trainer.generate(gen_data_iter)
        generate_fake(dec_samples)
        dis_data_iter = DisDataIter(POSITIVE, NEGATIVE, pad_token, BATCH_SIZE, SEQUENCE_LEN)
        for epoch in range(D_PRE_EPOCHS):
            loss = trainer.train_discriminator(dis_data_iter)
            print('Epoch [%d], loss: %f' % (epoch, loss))
            log.write_dis(str(epoch) + ';' + str(loss))
            writer.add_scalar('data/pre_d_loss', loss, (epoch))

        dis_data_iter = DisDataIter(POSITIVE, NEGATIVE, pad_token, BATCH_SIZE, SEQUENCE_LEN)
        result_real, result_fake = trainer.discriminate(dis_data_iter)
        print('1/1: %d / %d' % (sum(result_real), len(result_real)))
        print('1/0: %d / %d' % (sum(result_fake), len(result_fake)))
        generate_eval(vocabulary, result_fake)

        # Save pre trained models
        trainer.save(0, vocabulary)
        log.flush()

    ## Adversarial Training
    print('Start Adversarial Training...\n')
    for gan_epoch in range(trainer.get_epoch(), GAN_EPOCHS + 1):
        print('===== Adversarial Epoch %d / %d =====' % (gan_epoch, GAN_EPOCHS))
        ## Train the generator with policy gradient
        gen_data_iter.init_epoch()
        loss = trainer.train_gan(gen_data_iter, eos_token)
        print('GAN loss: %f' % (loss))
        log.write_gan(str(gan_epoch) + ';' + str(loss.data.cpu().numpy()))
        writer.add_scalar('data/gan_loss', loss, gan_epoch)

        # evaluate generator perplexity with test data
        gen_data_iter.init_epoch()
        loss = trainer.eval_generator(gen_data_iter)
        print('Perplexity: %f' % (loss))
        log.write_plp(str(G_PRE_EPOCHS + gan_epoch + 1) + ';' + str(loss))
        writer.add_scalar('data/perplexity', loss, G_PRE_EPOCHS + gan_epoch + 1)

        # Train the discriminator
        gen_data_iter.init_epoch()
        dec_samples = trainer.generate(gen_data_iter)
        generate_fake(dec_samples)
        dis_data_iter = DisDataIter(POSITIVE, NEGATIVE, pad_token, BATCH_SIZE, SEQUENCE_LEN)
        for d_epoch in range(D_EPOCHS):
            loss = trainer.train_discriminator(dis_data_iter)
            print('D epoch[%d], loss: %f' % (d_epoch, loss))
            log.write_dis_gan(str(gan_epoch) + ';' + str(d_epoch) + ';' + str(loss))
            iter = D_PRE_EPOCHS + (gan_epoch - 1) * D_EPOCHS + d_epoch
            writer.add_scalar('data/d_loss', loss, iter)

        # Print 10 rows of generated text
        dis_data_iter = DisDataIter(POSITIVE, NEGATIVE, pad_token, BATCH_SIZE, SEQUENCE_LEN)
        result_real, result_fake = trainer.discriminate(dis_data_iter)
        print('1/1: %d / %d' % (sum(result_real), len(result_real)))
        print('1/0: %d / %d' % (sum(result_fake), len(result_fake)))
        generate_eval(vocabulary, result_fake)
        print('=== EVAL.DATA sample (first 10 rows) ===')
        os.system('head -n 10 %s' %(EVAL_FILE))
        print(' ')

        # Save trained models
        if gan_epoch % opt.checkpoint_interval == 0:
            trainer.save(gan_epoch, vocabulary)

        log.flush()
    log.close()

if __name__ == '__main__':
    main()
