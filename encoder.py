# -*- coding: utf-8 -*-

import os
import random

import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.nn.utils.rnn as rnn

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

class Encoder(nn.Module):

    def __init__(self, vocab_size, embed_size, hidden_size, n_layers=1, dropout=0.5, bidirectional=False):
        super(Encoder, self).__init__()
        # Parameters
        self.vocab_size = vocab_size
        self.hidden_size = hidden_size
        self.embed_size = embed_size
        self.n_layers = n_layers
        self.direction = 2 if bidirectional else 1
        # Layers
        self.embedding = nn.Embedding(vocab_size, embed_size)
        self.gru = nn.GRU(embed_size, self.hidden_size, num_layers=n_layers,
                batch_first=True, bidirectional=bidirectional)

        self.init_params()

    def forward(self, input, length):
        """
        Args:
            input: (batch_size, seq_len), sequence of tokens to encode
            length: (batch_size), sequence of inputs length
        """
        batch_size = input.size(0)
        hidden = torch.zeros((self.direction, batch_size, self.hidden_size), device=device)
        embedded = self.embedding(input)
        packed = torch.nn.utils.rnn.pack_padded_sequence(embedded, length, batch_first=True)
        outputs, hidden = self.gru(packed, hidden)
        outputs, output_lengths = torch.nn.utils.rnn.pad_packed_sequence(outputs, batch_first=True)
        if self.direction == 2:
            hidden = torch.cat((hidden[0], hidden[1]), dim=1).unsqueeze(0)
        return outputs, hidden

    def init_params(self):
        for param in self.parameters():
            param.data.normal_(0, 0.1)

    def get_emb(self):
        return self.embedding.weight.data
